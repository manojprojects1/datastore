package roughnote;

public class SwapNumber {

	public static void main(String[] args) {
		int a=35;
		int b=70;
		System.out.println("before swap "+a);
		System.out.println("before swap "+b);
		
		int temp =a;
				a=b;
				b=temp;
		
		System.out.println("After swap "+a);
		System.out.println("After swap "+b);

	}

}
