package project;

public class Pattern1 {

	public static void main(String[] args) {
		for(int a=1;a<=5;a++) {
			for(int b=0;b<=4;b++) {
				System.out.print((a+b)%2+" ");
			}
			System.out.println();
		}

	}

}

//output
//1 0 1 0 1 
//0 1 0 1 0 
//1 0 1 0 1 
//0 1 0 1 0 
//1 0 1 0 1 
