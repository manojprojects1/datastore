package project;

public class Task2WhileLoop {

	public static void main(String[] args) {
		int rows=65;
		while(rows<=69)
		{
			int col=65;
			while(col<=69)
			{
				System.out.print((char)rows+" ");
				col=col+1;
			}
			System.out.println();
			rows=rows+1;
		}

	}

}
