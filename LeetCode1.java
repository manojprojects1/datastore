package Patterns;

public class LeetCode1 {

	public static void main(String[] args) 
	{
		String a="1010";
		  String b="1011";
		  
		  int c=Integer.parseInt(a,2);
		  int d=Integer.parseInt(b,2);
		  int add = c+d;
		  
		  String sum=Integer.toBinaryString(add);
		  
		  System.out.println(sum);
		  //output-10101.

	}

}
