package learnFile;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class FileHandling {

	public static void main(String[] args)throws Exception {
		File Ex = new File("/home/manoj/Desktop/new.txt");
		Ex.createNewFile();
		
		
		
		FileWriter pen = new FileWriter(Ex,true);
		pen.write("m");
		pen.write("a");
		pen.write("n");
		pen.write("o");
		pen.write("j");
		pen.close();
		
		
		FileReader meth = new FileReader(Ex);
		 int save =meth.read();
		 while(save!=-1) {
			 System.out.println(save);
			 save=meth.read();
		 }
		
		
		
	}

}
