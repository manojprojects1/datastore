package Patterns;

public class ConcateArray {

	public static void main(String[] args) {
		{
	        int[] a= {1,2,3,4,5};
	        int[] b= {1,3,5};
	        int[] c= new int [a.length+b.length];
	         
	         for(int i=0;i<c.length;i++)
	         {
	        	 if(i<a.length) 
	        	 {
	        	 c[i]=a[i];
	        	 System.out.print(c[i]);
	        	 }
	        	 else {
	        		 c[i]=b[i-a.length];
	        		 System.out.print(c[i]);
	        	 }
	        	 
	         }
	        

	}
	}
}
