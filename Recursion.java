package ArrayP;

public class Recursion {

	public static void main(String[] args) {
		int result = find_fact(10);
        System.out.println(result);
	}

	private static int find_fact(int i) {
		if(i==1) {
			return i * find_fact(i-1);
			
		}
		else
		{
		return 0;
		}
	}

}
