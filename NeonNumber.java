package Patterns;

public class NeonNumber {

	public static void main(String[] args) {

		int no = 94;
		int digit = 0;
		int sqr = 0;

		for (int i = 0; i < no; i++) {
			digit = 0;
			sqr = 0;
			while (no > 0) {
				digit = no % 10;
				sqr += (digit * digit);
				no = no / 10;
			}
//		System.out.println(sqr);
			no = sqr;

		}

		if (sqr == 1) {
			System.out.println("happy number");
		} else {
			System.out.println("not happy number");
		}

	}

}
