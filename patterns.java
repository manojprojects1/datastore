package roughnote;

public class patterns {

	public static void main(String[] args) {
		patterns ss = new patterns();

		   ss.pattern1();
		   ss.pattern2();
	    	
	}

	private void pattern2() {
		int no =5;
		for(int i=no;i>=0;i--) {
			for(int j=0;j<=i;j++) {
				if(j!=0) {
					System.out.print("*"+" ");
				}
				else {
					System.out.print(i+" ");
					
				}
				
			}
			System.out.println();
			
		}
		
	}

	private void pattern1() {
		for(int i=5;i>=1;i--) {
			for(int j=1;j<=i;j++) {
				if(j==i) {
					System.out.print(i+" ");
				}
				else {
					System.out.print("*"+" ");
				}
			}
			System.out.println();
		}
		
	}

}
