package com.payilagam.Library.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity

@Table(name="libraryManage")
public class libraryEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int s_no;
	private String book_name;
	private String author;
	private int qty;
	private int price_qty;
	public int getS_no() {
		return s_no;
	}
	public void setS_no(int s_no) {
		this.s_no = s_no;
	}
	public String getBook_name() {
		return book_name;
	}
	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public int getPrice_qty() {
		return price_qty;
	}
	public void setPrice_qty(int price_qty) {
		this.price_qty = price_qty;
	}
}
