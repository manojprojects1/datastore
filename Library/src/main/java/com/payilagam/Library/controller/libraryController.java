package com.payilagam.Library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.payilagam.Library.Entity.libraryEntity;
import com.payilagam.Library.Repository.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



@Controller
public class libraryController {

	@Autowired
	Repository repo;
	
	
	@GetMapping("dex")
	public ModelAndView method() {
		
		ModelAndView mv = new ModelAndView("library");
		mv.addObject("lib",repo.findAll());
		return mv;
		
	}
	
	@GetMapping("/add")
	public ModelAndView getMethodName(libraryEntity le) {
		
		ModelAndView mv = new ModelAndView("BookStore");
	     mv.addObject("bookadd",le);
		return mv;
	}
	
	
	@PostMapping("/add")
	public String postMethodName(libraryEntity Postle) {
		
		repo.save(Postle);
		return "redirect:/dex";
	}
	@GetMapping("alter/{id}")
	public ModelAndView ModelAndView  (@PathVariable int id) {
		ModelAndView man = new ModelAndView("lib");
		man.addObject("albk",repo.findById(id).get());		
		return man;
	}
	@PostMapping("alter/{id}")
	public String  savealt(@PathVariable int id,libraryEntity mk) {
		mk.setS_no(id);
		repo.save(mk);
		
		return "redirect:/dex";
	}
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable int id) {
       repo.deleteById(id);		
		
		return "redirect:/dex";
	}
	
	
	
	
	
}
