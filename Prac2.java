package project;     	      
	     

public class Prac2{
	
	public static void main(String[] args) {
		//loops
		  int i=0;
		  while(i<4) {
			  System.out.print(1+" ");
			  i++;
		  }
		  System.out.println();
		  
		  int j=0;
		  while(j<4) {
			  System.out.print(2+" ");
			  j++;
		  }
		  System.out.println();
		  
		  int k=0;
		  while(k<4) {
			  System.out.print(3+" ");
			  k++;
		  }
		  System.out.println();
		  
		  int l=0;
		  while(l<4) {
			  System.out.print(4+" ");
			  l++;
		  }
		  System.out.println();
		  System.out.println("==================");
		  
		  
		  //while loop
		  int m=1;
		  while(m<=4) {
			  int n=1;
			  while(n<=4) {
				  System.out.print(m+" ");
				  n++;
			  }
			  System.out.println();
			  m++;
		  }
		  System.err.println();
		  System.out.println("==============");
		  
		  //for loop
		  for(int a=1;a<=4;a++) {
			  for(int b=1;b<=4;b++) {
				  System.out.print(b+" ");
			  }
			  System.out.println();
		  }
		  
		  
	  
		      
	   
	
	}	
}

