package project;

public class Inc_decTasks {

	public static void main(String[] args) {
		Inc_decTasks task = new Inc_decTasks();
		task.task1();
		task.task2();

	}
	public void task1() {
		int no =5;
		  System.out.println(no++ + --no + no-- * ++no/no--);
	}
     public void task2() {
    	 int a = 11;
    	 int b = 22;
    	 System.out.println(a+b + a++ + b++ + ++a + ++b);
     }
}
