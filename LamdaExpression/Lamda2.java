package LambaExpression;

@FunctionalInterface
public interface Lamda2 {

	public int send(int amt);
}
