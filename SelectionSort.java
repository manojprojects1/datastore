package sorts;

import java.util.Arrays;

public class SelectionSort {

	public static void main(String[] args) {
		int[] num= {4,7,1,8,2};

		for(int i=0;i<num.length-1;i++)
		{
	        int  temp=i;
		for(int j=i+1;j<num.length;j++) 
		{
			if(num[temp]>num[j]) 
			{
				temp=j;
			}
			int small = num[temp];
			num[temp]=num[i];
			num[i]=small;
			
		}
	}
		System.out.println(Arrays.toString(num));

	}

}
